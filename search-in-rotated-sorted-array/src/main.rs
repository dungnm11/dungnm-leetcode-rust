struct Solution {}

impl Solution {
    pub fn search(nums: Vec<i32>, target: i32) -> i32 {
        if nums.len() < 5 {
            for i in 0..nums.len() {
                if nums[i] == target {
                    return i as i32;
                }
            }

            return -1;
        }

        let pivot = Solution::find_pivot(&nums, 0, nums.len() - 1);
        // println!("Pivot: {}", pivot);
        if pivot < 0 {
            return Solution::binary_search(&nums, 0, nums.len() - 1, target);
        }

        let pivot = pivot as usize;
        let left = Solution::binary_search(&nums, 0, pivot, target);
        if left >= 0 {
            return left;
        }

        let right = Solution::binary_search(&nums, pivot + 1, nums.len() - 1, target);
        if right >= 0 {
            return right;
        }

        -1
    }

    fn binary_search(nums: &Vec<i32>, from: usize, to: usize, target: i32) -> i32 {
        // println!("from: {}, to: {}", from, to);
        if to - from < 2 {
            if nums[from] == target {
                return from as i32;
            }
            if nums[to] == target {
                return to as i32;
            }
            return -1;
        }

        let mid: usize = (from + to) / 2;
        if nums[mid] > target {
            return Solution::binary_search(nums, from, mid, target);
        }

        Solution::binary_search(nums, mid, to, target)
    }

    fn find_pivot(nums: &Vec<i32>, from: usize, to: usize) -> i32 {
        // println!("from: {}, to: {}", from, to);
        if nums[from] < nums[to] {
            return -1;
        }

        if to - from < 2 {
            if nums[to] > nums[from] {
                return to as i32;
            }

            return from as i32;
        }

        let mid: usize = (from + to) / 2;
        let left = Solution::find_pivot(nums, from, mid);
        if left >= 0 {
            return left;
        }

        Solution::find_pivot(nums, mid, to)
    }
}

mod tests {
    use super::Solution;

    #[test]
    fn test_search1() {
        let nums = vec![4, 5, 6, 7, 0, 1, 2];
        let target = 0;
        assert_eq!(Solution::search(nums, target), 4);
    }

    #[test]
    fn test_search2() {
        let nums = vec![4, 5, 6, 7, 0, 1, 2];
        let target = 3;
        assert_eq!(Solution::search(nums, target), -1);
    }

    #[test]
    fn test_search3() {
        let nums = vec![1];
        let target = 0;
        assert_eq!(Solution::search(nums, target), -1);
    }

    #[test]
    fn test_search4() {
        let nums = vec![1, 2, 3, 4, 5, 6, 7, 0];
        let target = 0;
        assert_eq!(Solution::search(nums, target), 7);
    }

    #[test]
    fn test_search5() {
        let nums = vec![9, 1, 2, 3, 4, 5, 6, 7, 8];
        let target = 9;
        assert_eq!(Solution::search(nums, target), 0);
    }
}
fn main() {}
