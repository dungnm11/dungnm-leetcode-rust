struct Solution {}

use std::collections::HashMap;
use std::iter::FromIterator;

impl Solution {
    pub fn group_anagrams(strs: Vec<String>) -> Vec<Vec<String>> {
        if strs.len() == 1 {
            return vec![vec![strs[0].clone()]];
        }

        let mut map: HashMap<String, Vec<String>> = HashMap::new();
        for str in strs {
            let mut chars: Vec<char> = str.chars().collect();
            chars.sort_by(|a, b| b.cmp(a));
            let ordered_str = String::from_iter(chars);
            let key = ordered_str.clone();
            if !map.contains_key(&ordered_str) {
                map.insert(ordered_str, vec![]);
            }

            map.get_mut(&key).unwrap().push(str);
        }

        println!("map: {:?}", map);
        map.into_values().collect()
    }
}

mod tests {
    use super::Solution;

    #[test]
    fn test_group_anagrams1() {
        let input: Vec<String> = vec![
            String::from("eat"),
            String::from("tea"),
            String::from("tan"),
            String::from("ate"),
            String::from("nat"),
            String::from("bat"),
        ];
        let output = Solution::group_anagrams(input);
        assert_eq!(output.len(), 3);
    }

    #[test]
    fn test_group_anagrams2() {
        let input = vec![String::from("eat")];
        let output = Solution::group_anagrams(input);
        assert_eq!(output.len(), 1);
    }
}

fn main() {}
