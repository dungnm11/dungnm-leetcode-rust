struct Solution {}

impl Solution {
    pub fn combination_sum(candidates: Vec<i32>, target: i32) -> Vec<Vec<i32>> {
        let mut result: Vec<Vec<i32>> = vec![];

        for i in 0..candidates.len() {
            Solution::get_sum(&candidates, vec![candidates[i]], i, target, &mut result);
        }

        result
    }

    fn get_sum(
        can: &Vec<i32>,
        sum_vec: Vec<i32>,
        from: usize,
        target: i32,
        result: &mut Vec<Vec<i32>>,
    ) {
        let sum: i32 = sum_vec.iter().sum();
        if sum > target {
            return;
        }

        if sum == target {
            result.push(sum_vec);
            return;
        }

        for i in from..can.len() {
            let mut new_sum_vec = sum_vec.clone();
            new_sum_vec.push(can[i]);
            Solution::get_sum(can, new_sum_vec, i, target, result);
        }
    }
}

mod tests {
    use super::Solution;

    #[test]
    fn test_combination_sum1() {
        let target = 8;
        let output = Solution::combination_sum(vec![2, 3, 5], target);
        assert_eq!(output.len(), 3);
        for v in output {
            assert_eq!(sum_of_vec(v), target);
        }
    }

    #[test]
    fn test_combination_sum2() {
        let target = 1;
        let output = Solution::combination_sum(vec![2], target);
        assert_eq!(output.len(), 0);
    }

    fn sum_of_vec(v: Vec<i32>) -> i32 {
        v.iter().sum()
    }
}

fn main() {}
