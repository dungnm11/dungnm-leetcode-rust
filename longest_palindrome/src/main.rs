struct Solution {}

impl Solution {
    pub fn longest_palindrome(s: String) -> String {
        let s_n = s.split("").collect::<Vec<&str>>().join("|");

        let mut center = 0;
        let mut radius = 0;
        let mut radius_v: Vec<usize> = vec![];
        while center < s_n.len() {
            while center + radius < s_n.len() - 1
                && center - radius >= 1
                && s_n.chars().nth(center + radius + 1).unwrap()
                    == s_n.chars().nth(center - radius - 1).unwrap()
            {
                radius = radius + 1;
            }

            radius_v.push(radius);
            let old_center = center;
            let old_radius = radius;
            radius = 0;
            center = center + 1;
            while center <= old_center + old_radius {
                let mirror_center = old_center - (center - old_center);
                let max_mirror_radius = old_center + old_radius - center;
                if radius_v[mirror_center] < max_mirror_radius {
                    radius_v.push(radius_v[mirror_center]);
                    center = center + 1;
                } else if radius_v[mirror_center] > max_mirror_radius {
                    radius_v.push(max_mirror_radius);
                    center = center + 1;
                } else {
                    radius = max_mirror_radius;
                    break;
                }
            }
        }

        println!("{}", s_n);
        println!("{:?}", radius_v);
        return String::from("");
    }
}

fn main() {
    println!("{}", Solution::longest_palindrome(String::from("babad")));
}
