struct Solution {}

impl Solution {
    pub fn is_valid(s: String) -> bool {
        let mut stack: Vec<char> = vec![];
        let chars = s.chars();
        for c in chars {
            if c == '(' || c == '{' || c == '[' {
                stack.push(c);
            } else {
                println!("{:?}", stack);
                let last_char = stack.pop();
                if last_char == None {
                    return false;
                } else if c == ')' && last_char != Some('(') {
                    return false;
                } else if c == '}' && last_char != Some('{') {
                    return false;
                } else if c == ']' && last_char != Some('[') {
                    return false;
                }
            }
        }

        stack.is_empty()
    }
}

mod tests {
    use super::Solution;

    #[test]
    fn test_is_valid() {
        assert_eq!(Solution::is_valid(String::from("()")), true);
        assert_eq!(Solution::is_valid(String::from("()[]{}")), true);
        assert_eq!(Solution::is_valid(String::from("(]")), false);
        assert_eq!(Solution::is_valid(String::from("(((((")), false);
        assert_eq!(Solution::is_valid(String::from("(({[]})())")), true);
        assert_eq!(Solution::is_valid(String::from(")))(((")), false);
        assert_eq!(Solution::is_valid(String::from("([)]")), false);
        assert_eq!(Solution::is_valid(String::from("{[]}")), true);
    }
}

fn main() {
    let output = Solution::is_valid(String::from("(]"));
    println!("{}", output);
}
