use std::collections::{HashMap, HashSet};

struct Solution {}

impl Solution {
    pub fn three_sum(mut nums: Vec<i32>) -> Vec<Vec<i32>> {
        let mut result: Vec<Vec<i32>> = vec![];
        nums.sort();
        // println!("{:?}", nums);

        if nums.len() < 3 {
            return vec![];
        }

        for i in 0..(nums.len() - 1) {
            if i > 0 && nums[i] == nums[i - 1] {
                continue;
            }

            let mut left = i + 1;
            let mut right = nums.len() - 1;
            // println!("i = {}", i);
            while left < right {
                // println!("left: {}, right: {}", left, right);
                let sum = nums[i] + nums[left] + nums[right];
                if sum > 0 {
                    right -= 1;
                    continue;
                } else if sum < 0 {
                    left += 1;
                    continue;
                }

                result.push(vec![nums[i], nums[left], nums[right]]);
                // println!("pushed");
                left += 1;
                while left < right && nums[left] == nums[left - 1] {
                    left += 1;
                }
            }
        }

        result
    }

    pub fn three_sum_without_order(nums: Vec<i32>) -> Vec<Vec<i32>> {
        let mut map: HashMap<&i32, HashSet<usize>> = HashMap::new();
        let mut result_set: HashSet<(i32, i32, i32)> = HashSet::new();
        for i in 0..nums.len() {
            let num = &nums[i];
            if !map.contains_key(num) {
                map.insert(num, HashSet::new());
            }

            map.get_mut(num).unwrap().insert(i);
        }

        for i in 0..nums.len() {
            for j in (i + 1)..nums.len() {
                let value = -(nums[i] + nums[j]);
                if !map.contains_key(&value) {
                    continue;
                }

                let set = map.get(&value).unwrap();
                let mut count = 0;
                if set.contains(&i) {
                    count = count + 1;
                }
                if set.contains(&j) {
                    count = count + 1;
                }

                if count >= set.len() {
                    continue;
                }

                result_set.insert(Solution::three_to_ordered_tuple(nums[i], nums[j], value));
            }
        }

        let mut result: Vec<Vec<i32>> = vec![];
        for re in result_set.into_iter() {
            let (r1, r2, r3) = re;
            result.push(vec![r1, r2, r3]);
        }

        result
    }

    fn three_to_ordered_tuple(a: i32, b: i32, c: i32) -> (i32, i32, i32) {
        if a > b {
            if b > c {
                return (a, b, c);
            } else {
                if a > c {
                    return (a, c, b);
                } else {
                    return (c, a, b);
                }
            }
        } else {
            if a > c {
                return (b, a, c);
            } else {
                if b > c {
                    return (b, c, a);
                } else {
                    return (c, b, a);
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Solution;

    #[test]
    fn test_three_sum_without_order() {
        let input = vec![-1, 0, 1, 2, -1, -4];
        let output = Solution::three_sum_without_order(input);
        assert_eq!(output.len(), 2);

        let input = vec![2, -1, 0, 1, 2, -1, -4];
        let output = Solution::three_sum_without_order(input);
        assert_eq!(output.len(), 3);

        let input = vec![-1, 0, 1, 2, -1, -4, -2, -3, 3, 0, 4];
        let output = Solution::three_sum_without_order(input);
        assert_eq!(output.len(), 9);
    }

    #[test]
    fn test_three_sum() {
        let input = vec![-1, 0, 1, 2, -1, -4];
        let output = Solution::three_sum(input);
        assert_eq!(output.len(), 2);

        let input = vec![2, -1, 0, 1, 2, -1, -4];
        let output = Solution::three_sum(input);
        assert_eq!(output.len(), 3);

        let input = vec![-1, 0, 1, 2, -1, -4, -2, -3, 3, 0, 4];
        let output = Solution::three_sum(input);
        assert_eq!(output.len(), 9);

        let input = vec![-2, 0, 1, 1, 2];
        let output = Solution::three_sum(input);
        assert_eq!(output.len(), 2);
    }
}

fn main() {
    let input = vec![-1, 0, 1, 2, -1, -4];
    Solution::three_sum(input);
}
