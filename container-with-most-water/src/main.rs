struct Solution {}

impl Solution {
    pub fn max_area(height: Vec<i32>) -> i32 {
        let length = height.len();
        let mut max = 0;
        for i in 0..(length - 1) {
            // println!("col: {}", i);
            let h1 = height[i];

            if (h1 * (length - 1 - i) as i32) < max {
                continue;
            }

            for j in ((i + 1)..length).rev() {
                let h2 = height[j];
                let w = (j - i) as i32;
                // println!("h2: {}, w: {}", h2, w);
                if h1 > h2 {
                    let area = h2 * w;
                    if area > max {
                        max = area;
                    }
                } else {
                    let area = h1 * w;
                    if area > max {
                        max = area;
                    }
                    break;
                }
            }
        }

        max
    }
}


mod tests {
    #[test]
    fn testcase1() {
        let re = super::Solution::max_area([1, 8, 6, 2, 5, 4, 8, 3, 7].to_vec());
        assert_eq!(re, 49);
    }

    #[test]
    fn testcase2() {
        let re = super::Solution::max_area([1, 1].to_vec());
        assert_eq!(re, 1);
    }

    #[test]
    fn testcase3() {
        let re = super::Solution::max_area([2, 3, 4, 5, 18, 17, 6].to_vec());
        assert_eq!(re, 17);
    }

    #[test]
    fn testcase4() {
        let re = super::Solution::max_area([1, 1, 1, 1, 1, 1, 1, 50, 50, 1, 1, 1, 1, 1, 1, 1, 1].to_vec());
        assert_eq!(re, 50);
    }
}

fn main() {
    Solution::max_area([1, 1].to_vec());
}

