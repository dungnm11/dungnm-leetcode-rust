use std::collections::HashMap;
use std::convert::TryFrom;

struct Solution {}

impl Solution {
    pub fn two_sum(nums: Vec<i32>, target: i32) -> Vec<i32> {
        let mut num_map: HashMap<i32, i32> = HashMap::new();

        for (i, n) in nums.into_iter().enumerate() {
            let index32: i32 = match i32::try_from(i).ok() {
                None => panic!("1"),
                Some(value) => value,
            };

            let num_mapped = num_map.get(&n);
            if num_mapped.is_some() {
                return vec![index32, num_mapped.unwrap().clone()];
            }

            let remain = target - n;
            num_map.insert(remain, index32);
        }

        panic!("NO");
    }
}

fn main() {
    let v = vec![2, 7, 11, 15];
    let so = Solution::two_sum(v, 9);
    println!("{:?}", so);

    let v = vec![3, 2, 4];
    let so = Solution::two_sum(v, 6);
    println!("{:?}", so);

    let v = vec![3, 3];
    let so = Solution::two_sum(v, 6);
    println!("{:?}", so);
}
