struct Solution {}

impl Solution {
    pub fn rotate(matrix: &mut Vec<Vec<i32>>) {
        let mut m_size: usize = matrix.len() - 1;
        let layers: usize = matrix.len() / 2;

        for l in 0..layers {
            for i in l..matrix.len() - 1 - l {
                //println!("{:?}, {:?}, {:?}, {:?}", (i, l), (m_size, i), (m_size - i + l, m_size), (l, m_size - i + l));
                Solution::swap((i, l), (m_size, i), matrix);
                Solution::swap((i, l), (m_size - i + l, m_size), matrix);
                Solution::swap((i, l), (l, m_size - i + l), matrix);
            }

            m_size -= 1;
            //println!("-----end of layer-----");
        }

        //println!("{:?}", matrix);
    }

    fn swap(from: (usize, usize), to: (usize, usize), matrix: &mut Vec<Vec<i32>>) {
        let (x, y) = from;
        let (x_, y_) = to;
        //println!("swap value {} to {}", matrix[y][x], matrix[y_][x_]);
        let temp = matrix[y][x];
        matrix[y][x] = matrix[y_][x_];
        matrix[y_][x_] = temp;
    }
}

mod tests {
    use super::Solution;

    #[test]
    fn test_rotate1() {
        let mut input = vec![vec![1,2,3],vec![4,5,6],vec![7,8,9]];
        Solution::rotate(&mut input);
        let expected_output = vec![vec![7,4,1],vec![8,5,2],vec![9,6,3]];
        for i in 0..input.len() {
            assert_eq!(input[i], expected_output[i]);
        }
    }

    #[test]
    fn test_rotate2() {
        let mut input = vec![vec![5,1,9,11],vec![2,4,8,10],vec![13,3,6,7],vec![15,14,12,16]];
        Solution::rotate(&mut input);
        let expected_output = vec![vec![15,13,2,5],vec![14,3,4,1],vec![12,6,8,9],vec![16,7,10,11]];
        for i in 0..input.len() {
            assert_eq!(input[i], expected_output[i]);
        }
    }
}
fn main() { }
